#!/usr/bin/env python

import subprocess
import os

uaidir = '/usr/userfs/j/jc33/local/research/mpe/data/uai/'
eviddir = '/usr/userfs/j/jc33/local/research/mpe/data/evid/'

files = subprocess.check_output(['ls', uaidir]).split()
for f in files:
    if f[-4:] != '.uai':
        continue
    if os.path.isfile(f+'.log'):
        continue
    try:
        subprocess.call(['ilp_map.py', '-l', f+'.log', '-u', '-n', uaidir+f, eviddir+f+'.evid'])
    except:
        pass
