#!/usr/bin/env python
'''Uses CPLEX to solve MAP problems

Global varibles:
p:             Number of MRF variables
variables:     Tuple of MRF variables (0,1, .. , p-1)
arity:         arity[v] is the arity of variable v
n_cliques:     Number of cliques
cliques:       Tuple of cliques (0,1, .. , n_cliques-1)
zeroes:        Set of (clique,clique_inst) pairs corresponding to zero probability partial instantiations
               due to evidence or zeroes in the potential
markov:        The input file for the MRF
vs:            vs[c] is a tuple of variables in clique c in the order given in the input file
setvs:         setvs[c] is the variables in clique c as a set
star:          star[v] is the set of cliques containing variable v
tables:        tables[c][ci] contains the log probability for instantiation with index ci in clique c
evid:          Input file for evidence (if any)
intersections: intersections[s] is a set of cliques each pair of which has s as (a subset of) their intersection.
marg:          marg[c][ci][i] is the value of the ith variable in clique c in clique instantation index ci
varname:       varname[c][ci] is the name for the ILP variable corresponding to clique instantiation ci in clique c 
               note that we can have varname[c1][ci1] = varname[c2][ci2], which is OK since both are the same partial instantiation
varindex:      If var_index[c][ci] = i, for clique c, clique inst ci, then the ith MIP variable is the one for this partial instantiation
               var_index[c][ci] = None for deleted MIP variables
Main output:
A "joingraph" of cliques, each edge corresponding to a set of constraints of consistent marginal probabilities for the variables
which form the intersection of the two cliques (one constraint for each instantiation of the common variables).
The edge is labelled with this intersection (equivalently there is a separator on the edge).
If c1 and c2 have intersection s, then there is a path from c1 to c2 such that each edge on the path is labelled
with a set which is a superset of s.


Only certain join graphs are allowed. For example, with cliques ABC,AF,ABE,BD these edges form a joingraph according to Darwiche\'s definition:
ABC-AF,ABC-BD,ABE-AF,ABE-BD but is not allowed here since no path from ABC to ABE labelled AB
We need this:
AF-ABC,ABC-ABE,ABE-BD

'''

import sys
from itertools import product
from math import log

import cplex
from cplex.callbacks import IncumbentCallback


import argparse
parser = argparse.ArgumentParser(description='Use CPLEX to solve MAP problems in .uai format')
parser.add_argument('-v', '--verbosity', action="count",
                    help="increase output verbosity")
parser.add_argument('-u', '--uni', action='store_true', help = 'Use constraints on single variable values')
parser.add_argument('-i', '--incumbent', action='store_true', help = 'Write incumbents to file in .uai format')
parser.add_argument('-t', '--time', type=int, default=2000, help = 'Time limit on solving in seconds (default = 1000)')
parser.add_argument('-o', '--out', action='store_true', help = 'Solver output sent to stdout rather than a log file')
parser.add_argument('-n', '--nojoint', action='store_true', help='Skip constraints on consistent marginals between cliques')
parser.add_argument('-w', '--writeprob', action='store_true', help='Whether to write orginal problem to a .lp file')
parser.add_argument('-c', '--oneinstperclique', action='store_true', help='Post exactly one inst per clique constraints')
parser.add_argument('-q', '--eqs', action='store_true', help='Link single variable values to clique insts by equations (rather than set partitioning constraints)')
parser.add_argument('-r', '--redundantuse', action='store_true', help='Include redundant constraints when linking single variable values to clique insts')
#parser.add_argument('-k', '--omitlast', action='store_true', help='For constraints linking single variable to clique insts, whether to omit constraint for last value of variable')
parser.add_argument('-s', '--simp', type=int, default=2, choices=[0,1,2], help = 'The degree to which this script removes redundant equations, can be either 0, 1 or 2 (the default)')
parser.add_argument('-p', '--printsol', action='store_true', help =' Whether to print solution to standard output')
parser.add_argument('-l', '--log', default='', help ='alternative log filename')
parser.add_argument('-e', '--evid', action='store_true', help ='Assume evidence file is \'markov\'.evid')
parser.add_argument('markov', help='Markov network in .uai format')
parser.add_argument('evidence', help='Evidence for the Markov network in .uai format', nargs='?', default=None)
args = parser.parse_args()

verbose = False
very_verbose = False
if args.verbosity > 0:
   verbose = True
   if args.verbosity > 1:
      very_verbose = True


class MyIncumbentCallback(IncumbentCallback):

   first_incumbent = True

   def __call__(self):
      inst = [None]*p
      # extract instantiation for each variable
      for c in cliques:
         for ci, ci_varname in enumerate(varname[c]):
            if (c,ci) in zeroes:
               continue
            if self.get_values([ci_varname])[0] > 0.5:
               clique_inst = marg[c][ci]
               for i, v in enumerate(vs[c]):
                  inst[v] = clique_inst[i]
               break
      if not self.first_incumbent:
         print >>result, '-BEGIN-'
      else:
         self.first_incumbent = False
      print >>result, p,
      for val_inst in inst:
         print >>result, val_inst,
      print >>result

      
def marginal(variables,clique):
   '''for each instantiation of the variables
   in variables return a list of indices of those instantiations
   in clique for which it is consistent
   '''
   indx = {}
   for i, v in enumerate(variables):
      indx[v] = i

   varset = set(variables)
   n = len(variables)
   varpos = [None]*n
   for i, v in enumerate(vs[clique]):
      if v in varset:
         varpos[indx[v]] = i

   dkt = {}
   this_inst = [None]*n
   for ci, vis in enumerate(marg[clique]):
      for i,pos in enumerate(varpos):
         this_inst[i] = vis[pos]
      try:
         dkt[tuple(this_inst)].append(ci)
      except KeyError:
         dkt[tuple(this_inst)] = [ci]
   return dkt


######################################################################
# START READING IN EVIDENCE ...
######################################################################
if args.evidence or args.evid:
   if args.evidence:
      evidfile = open(args.evidence)
   else:
      evidfile = open(args.markov+'.evid')
   evidfile.readline() #discard first line
   evidence = evidfile.readline().rstrip().split()
   evidfile.close()
   if verbose:
      print 'Evidence read in'
else:
   evidence = []
   if verbose:
      print 'No evidence supplied'
######################################################################
# ... END READING IN EVIDENCE 
######################################################################


######################################################################
# START READING IN MARKOV NET ...
######################################################################
markov = open(args.markov)    
if verbose:
   print 'Reading in problem ... ',
   sys.stdout.flush()
markov.readline().rstrip()

p = int(markov.readline())
variables = tuple(range(p))
arity = tuple([int(x) for x in markov.readline().rstrip().split()])
n_cliques = int(markov.readline())
cliques = tuple(range(n_cliques))
zeroes = set()
edges = set()

# vs[c] is a list of variables in clique c, setvs the set, star[v] is the set of cliques which contain v 
vs = [None]*n_cliques
setvs = [None]*n_cliques
star = {}
for v in variables:
   star[v] = set()
for c in cliques:
    # vs[c] is a list of variables in the clique, not necessarily ordered
    vs[c] = [int(v) for v in tuple(markov.readline().rstrip().split()[1:])]
    setvs[c] = frozenset(vs[c])
    for v in vs[c]:
       star[v].add(c)
vs = tuple(vs)
setvs = tuple(setvs)

# tables[c][ci] contains the coefficient for instantiation ci in clique c
tables = [None]*n_cliques
for c in cliques:
   line = markov.readline()
   while line.strip() == '':
      line = markov.readline()
   size = int(line)
   tablesc = [None]*size
   ci = 0
   lastfield = None
   allsame = True
   while ci < size:
      for field in markov.readline().split():
         try:
            tablesc[ci] = log(float(field))
         except ValueError:
            zeroes.add((c,ci))
             
         if allsame and lastfield is not None and field != lastfield:
            allsame = False
         else:
            lastfield = field
            
         ci += 1
      # if all objective values are the same
      # and we have no evidence then can ignore this clique
      # and leave this table as 'None'
      if not allsame or evidence != []:
         tables[c] = tuple(tablesc)
      elif verbose:
         print 'Ignoring clique', c
tables = tuple(tables)
markov.close()
if verbose:
   print 'Done'
######################################################################
# ... END READING IN MARKOV NET
######################################################################




######################################################################
# START STORING EVIDENCE IN 'ZEROES'
######################################################################

def update_zeroes(variable,value,zeroes):
   for this_clique in star[variable]:
      # find position of variable in ints of this clique
      for j, v in enumerate(vs[this_clique]):
         if v == variable:
            pos = j
            break
      ranges = (range(arity[v]) for v in vs[this_clique])
      for ci, vis in enumerate(product(*ranges)):
         if vis[pos] != value:
            zeroes.add((this_clique,ci))

# use evidence
fields = tuple([int(x) for x in evidence])
i = 1 # skip first field, it just states number of variables in evidence
while i < len(fields):
   variable, value = fields[i], fields[i+1]
   update_zeroes(variable,value,zeroes)
   i += 2
if verbose:
   print 'Eliminated MIP variables using evidence'


######################################################################
# END STORING EVIDENCE IN 'ZEROES'
######################################################################

if not args.nojoint:
   # intersections[s] is a set of cliques each pair of which has s as (a subset of) their intersection
   # for each clique c1 in intersections[s] there is at least one other clique c2 such that c1 & c2 = s
   intersections = {}
   for c1 in cliques:
      setvsc1 = setvs[c1]
      done = set()
      # only consider cliques c2 which intersect on at least one variable with c1 ... 
      for v in setvsc1:
         for c2 in star[v]:

            if (c2 in done) or (c2 <=  c1):
               continue

            this_intersection = setvsc1 & setvs[c2]
            try:
               intersections[this_intersection].update([c1,c2])
            except KeyError:
               intersections[this_intersection] = set([c1,c2])

            done.add(c2)
   if verbose:
      print 'Created clique intersections'
   # prune intersections
   biggest = 0
   for intersection in intersections:
      if len(intersection) > biggest:
         biggest = len(intersection)
   for intersection, these_cliques in intersections.items():
      togo = set()
      int_size = len(intersection)
      if int_size == biggest:
         continue
      tuple_cliques = tuple(these_cliques)
      for i, c1 in enumerate(tuple_cliques):
         if c1 in togo:
            continue
         setvsc1 = setvs[c1]
         for c2 in tuple_cliques[i+1:]:
            if c2 in togo:
               continue
            # c1 and c2 have a bigger intersection than this one
            # only need one of them
            if len(setvsc1 & setvs[c2]) > int_size:
               togo.add(c2)
      these_cliques -= togo
   if verbose:
      print 'Pruned clique intersections'


# construct varnames and marg
marg = [None]*n_cliques
varname = [None]*n_cliques
if verbose:
   print 'Creating MIP variable names ... ',
   sys.stdout.flush()

for c in cliques:
   clique_vars = vs[c]
   cvis = range(len(clique_vars))
   ranges = (range(arity[v]) for v in clique_vars)
   clique_marg = []
   clique_varname = []
   for ci, vis in enumerate(product(*ranges)):
      clique_marg.append(vis)
      if (c,ci) in zeroes:
         long_name = ''
      else:
         # if possible use a name which explicitly states the partial instantiation
         long_name = 'x'+'_'.join(['#{0}#{1}'.format(clique_vars[i],vis[i]) for i in cvis])
      if len(long_name) < 256: 
         clique_varname.append(long_name)
      else:
         clique_varname.append('y#{0}#{1}'.format(c,ci))
   marg[c] = tuple(clique_marg)
   varname[c] = tuple(clique_varname)
marg = tuple(marg)
varname = tuple(varname)

   

if verbose:
   print 'Done'


# create CPLEX instance
map_prob = cplex.Cplex()
# set as a maximisation problem
map_prob.objective.set_sense(map_prob.objective.sense.maximize)

# create variables and set objective coefficients
#import pdb; pdb.set_trace()
nvars = 0
var_index = [None] * n_cliques
if verbose:
   print 'Creating clique instantiation MIP variables ...'
for c in cliques:

   # ignore tables we can ignore
   if tables[c] is None:
      continue
   
   var_index_c = [None] * len(tables[c])
   c_varname = varname[c]
   c_obj = []
   c_names = []
   for ci, val in enumerate(tables[c]):
      if (c,ci) in zeroes:
         continue
      var_index_c[ci] = nvars
      nvars += 1
      c_obj.append(val)
      c_names.append(c_varname[ci])
   map_prob.variables.add(
      obj=c_obj,
      types=[map_prob.variables.type.binary]*len(c_obj),
      names=c_names)
   var_index[c] = tuple(var_index_c)
var_index = tuple(var_index)

# create variables for individual variable instantiations
# and add constraint that variable takes exactly one of its values
if args.uni:
   u_var_index = []
   if verbose:
      print 'Creating variable instantiation MIP variables ...'
   for v in variables:
      u_var_index_v = []
      for val in range(arity[v]):
         map_prob.variables.add(
            types=[map_prob.variables.type.binary],
            names=['v#{0}#{1}'.format(v,val)])
         u_var_index_v.append(nvars)
         nvars += 1
      map_prob.linear_constraints.add(
         lin_expr=[cplex.SparsePair(ind=u_var_index_v,val=[1.0]*arity[v])],
         senses=["E"],
         rhs=[1.0])
      u_var_index.append(tuple(u_var_index_v))
   u_var_index = tuple(u_var_index)

   # for u in u_var_index:
   #    for v in u:
   #       map_prob.order.set([(v, 10, map_prob.order.branch_direction.default)])
   
if verbose:
   print 'Created {0} MIP variables'.format(nvars)

if args.oneinstperclique:
   # Exactly one instantiation for each clique
   my_sparse_pairs = [None]*n_cliques
   for c in cliques:

      if tables[c] is None:
         continue
      
      cc_vars = [x for x in var_index[c] if x is not None] 
      if very_verbose:
         print 'Creating constraint for clique {0} with {1} insts'.format(c,len(cc_vars))
      my_sparse_pairs[c] = cplex.SparsePair(ind=cc_vars,val=[1.0]*len(cc_vars))
   if verbose:
      print 'Created {0} one inst per clique constraints'.format(n_cliques)
   map_prob.linear_constraints.add(
      lin_expr=my_sparse_pairs,
      senses=["E"]*n_cliques,
      rhs=[1.0]*n_cliques)
      #names=['convex_clique_{0}'.format(c) for c in cliques])


u_nma_cons = 0
nma_cons = 0

if args.uni:
   for c in cliques:

      if tables[c] is None:
         continue
      
      vsc = vs[c]
      # set up storage
      store = []
      for v in vsc:
         vstore = []
         for val in range(arity[v]):
            vstore.append([])
         store.append(vstore)

      ranges = (range(arity[v]) for v in vsc)
      poss = range(len(vsc))
      # go through each partial instantiation in the clique
      for ci, vis in enumerate(product(*ranges)):
         # and each variable in the clique
         for pos in poss:
            # if not ruled out by evidence
            if var_index[c][ci] is not None:
               # add ci (clique inst) to list of clique insts consistent with 'pos' having value vis[pos]
               store[pos][vis[pos]].append(var_index[c][ci])
      # post a constraint for each value of each variable
      for i, v in enumerate(vsc):
         vals = arity[v]
         for val in range(vals):
            # optionally skip adding some redundant constraints
            if not args.redundantuse and i > 0 and val == vals-1:
               continue
            if args.eqs:
               my_lin_expr = cplex.SparsePair(ind=[u_var_index[v][val]]+store[i][val],val=[-1.0]+[1.0]*len(store[i][val]))
               map_prob.linear_constraints.add(
                  lin_expr = [my_lin_expr],
                  senses = ["E"],
                  rhs = [0])
            else:
               othervals = [u_var_index[v][val2] for val2 in range(vals) if val2 != val]
               my_lin_expr = cplex.SparsePair(ind=othervals+store[i][val],val=[1.0]*len(othervals)+[1.0]*len(store[i][val]))
               map_prob.linear_constraints.add(
                  lin_expr = [my_lin_expr],
                  senses = ["E"],
                  rhs = [1])
            u_nma_cons += 1

if not args.nojoint:
   def keyfn(c):
      return len(vs[c])
   my_linexprs = []
   for intersection, these_cliques in intersections.items():
      these_cliques = sorted(these_cliques,key=keyfn)
      sorted_intersection = sorted(intersection)
      c1 = these_cliques[0]
      dkt1 = marginal(sorted_intersection,c1)
      for c2 in these_cliques[1:]:
         dkt2 = marginal(sorted_intersection,c2)
         i = 0
         for inst, cis1 in dkt1.items():
            # can skip one equation
            if i == 0:
               i = 1
               continue
            lhs = set([var_index[c1][ci] for ci in cis1 if var_index[c1][ci] is not None] )
            rhs = set([var_index[c2][ci] for ci in dkt2[inst] if var_index[c2][ci] is not None])
            common = lhs & rhs
            lhs = list(lhs - common)
            rhs = list(rhs - common)
            if lhs == [] and rhs == []:
               continue
            ma_name = 'marg_{0}_{1}_{2}:'.format(c1,c2,i)
            if very_verbose:
               print 'Creating constraint', ma_name
            my_linexprs.append(cplex.SparsePair(ind=lhs+rhs,val=([-1.0]*len(lhs))+([1.0]*len(rhs))))
            nma_cons += 1
            i += 1
   map_prob.linear_constraints.add(
      lin_expr=my_linexprs,
      senses=["E"]*nma_cons,
      rhs=[0.0]*nma_cons)

if verbose:
   print 'Created {0} marginal consistency constraints'.format(nma_cons + u_nma_cons)

if args.writeprob:
   map_prob.write(args.markov+".lp")
   
result = open(args.markov+'.MAP','w')
print >>result, 'MAP'
if not args.out:
   if args.log:
      map_prob.set_results_stream(args.log)
   else:
      map_prob.set_results_stream(args.markov+'.log')

map_prob.parameters.timelimit.set(args.time)
map_prob.parameters.emphasis.mip.set(map_prob.parameters.emphasis.mip.values.optimality)
#map_prob.parameters.preprocessing.dependency.set(3)
map_prob.parameters.mip.cuts.zerohalfcut.set(2)
map_prob.parameters.mip.limits.cutpasses.set(9999999)
map_prob.parameters.mip.limits.cutsfactor.set(90)

#map_prob.parameters.mip.cuts.cliques.set(3)

# added Mon 16 Jul 11:41:56 BST 2018
# due to what Kadioglu et al stated
# branch on most fractional variable X and explore X=1 first.
#map_prob.parameters.mip.strategy.variableselect.set(1)
#map_prob.parameters.mip.strategy.branch.set(1)

if args.incumbent:
   map_prob.register_callback(MyIncumbentCallback)

   
if verbose:
   print 'Solving ...',
   sys.stdout.flush()
map_prob.solve()
if verbose:
   print 'Done'

if args.printsol:
   for c in cliques:
      for ci, x in enumerate(var_index[c]):
         if (c,ci) in zeroes:
            continue
         print varname[c][ci], map_prob.solution.get_values([x])[0]


#inst = [None]*p
# extract instantiation for each variable
#for c in cliques:
#   for ci, x in enumerate(var_index[c]):
#      if (c,ci) in zeroes:
#         continue
#      if map_prob.solution.get_values([x])[0] > 0.5:
#         clique_inst = marg[c][ci]
#         for i, v in enumerate(vs[c]):
#            inst[v] = clique_inst[i]
#         break
#print >>result, p,
#for val_inst in inst:
#   print >>result, val_inst,
#print >>result

result.close()



